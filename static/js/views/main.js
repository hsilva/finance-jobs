App.objects.BaseView = function() {
	this.client = new Dropbox.Client({key: 'slrzwsp7zxkt64g'});
	this.$loginDropbox = $('.btn-login');
	
	this.initEvents();
}

App.objects.BaseView.prototype = {
	initEvents: function() {
		this.authenticateAccount();
		this.login();
	},

	authenticateAccount: function() {
		var that = this;
		this.client.authenticate({interactive: false}, function (error) {
		    if ( error ) {
		        alert('Authentication error: ' + error);
		    }
		});

		if ( this.client.isAuthenticated() ) {
			console.log( 'logado' );

			that.client.getAccountInfo(function (error, info) {
			    console.log('Name: ' + info.name);
			});	


			// Acesso Login
			accesstoken = this.client.credentials();
			console.log(accesstoken);
			this.client.setCredentials(accesstoken);

		}

		$('.sair').on('click', function() {
			that.client.signOff(function( options, callback ) {
				console.log(options);
				console.log(callback);
			});			
		});


	},

	login: function () {
		var that = this;

		this.$loginDropbox.on('click', function( event ) {
			event.preventDefault();

			that.client.authenticate();
		});
	}
}

$(function() {
	App.objects.baseView = new App.objects.BaseView();
});