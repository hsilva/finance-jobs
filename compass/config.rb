static_dir = "../static/"

css_dir = static_dir +  "css/"
sass_dir = "source"
images_dir = static_dir + "img/"
javascripts_dir = static_dir + "js/"
fonts_dir = static_dir + "font/"
generated_images_dir = images_dir + "common/"

output_style = :compressed
relative_assets = true
line_comments = false
sass_options = { :debug_info => false }

http_path = "127.0.0.1:8000/"
http_images_path = "/finance-jobs/img/"
http_generated_images_path = "/finance-jobs/img/"   